/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managers;

import classes.adminComment;
import java.util.ArrayList;

/**
 *
 * @author Aidan
 */
public class commentManager {
    private static ArrayList<adminComment> listOfComments = new ArrayList<>();

    public static ArrayList<adminComment> getListOfComments() {
        return listOfComments;
    }
    
    public void addComment(adminComment newComment)
    {
        listOfComments.add(newComment);
    }
    
}
