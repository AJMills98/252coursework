/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managers;

import classes.doctorFeedback;
import java.util.ArrayList;

/**
 *
 * @author Aidan
 */
public class feedbackManager {
    private static ArrayList<doctorFeedback> listOfFeedback = new ArrayList<>();

    public ArrayList<doctorFeedback> getListOfFeedback() {
        return listOfFeedback;
    }
    
    public void addFeedback(doctorFeedback newFeedback){
        listOfFeedback.add(newFeedback);
    }
    
    
}
