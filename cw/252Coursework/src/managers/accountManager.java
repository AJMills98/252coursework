/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managers;

import classes.admins;
import classes.doctors;
import classes.logins;
import classes.patients;
import classes.secretary;
import java.util.ArrayList;

/**
 *
 * @author Aidan
 */
public class accountManager {
    private static ArrayList<logins> listOfLogins = new ArrayList<>();
    private static ArrayList<doctors> listOfDoctors = new ArrayList<>();
    private static ArrayList<admins> listOfAdmins = new ArrayList<>();
    private static ArrayList<patients>listOfPatients = new ArrayList<>();
    private static ArrayList<secretary>listOfSecretary = new ArrayList<>();

    public ArrayList<logins> getListOfLogins() {
        return listOfLogins;
    }

    public ArrayList<doctors> getListOfDoctors() {
        return listOfDoctors;
    }

    public ArrayList<admins> getListOfAdmins() {
        return listOfAdmins;
    }

    public ArrayList<patients> getListOfPatients() {
        return listOfPatients;
    }

    public ArrayList<secretary> getListOfSecretary() {
        return listOfSecretary;
    }
    
    public void addDoctor(doctors newDoctor){                 
        listOfDoctors.add(newDoctor);
    }
    public void addLogin(logins newLogin){
        listOfLogins.add(newLogin);
    }
    public void addAdmin(admins newAdmin){
        listOfAdmins.add(newAdmin);
    }
    public void addPatient(patients newPatient){
        listOfPatients.add(newPatient);
    }
    public void addSecretary(secretary newSecretary){
        listOfSecretary.add(newSecretary);
    }
    
}
