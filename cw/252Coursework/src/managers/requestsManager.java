/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managers;

import classes.patientRequests;
import java.util.ArrayList;

/**
 *
 * @author Aidan
 */
public class requestsManager {
    private static ArrayList<patientRequests> listOfRequests = new ArrayList<>();

    public ArrayList<patientRequests> getListOfRequests() {
        return listOfRequests;
    }
    
    public void addRequest (patientRequests newRequest)
    {
        listOfRequests.add(newRequest);
    }

}
