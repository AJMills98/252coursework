/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import managers.accountManager;
import guis.adminPage;
import guis.loginPage;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import managers.feedbackManager;
import managers.medicineManager;
import managers.patientManager;


/**
 
 * @author AidanA
 */
public class Main{
    

    public static void main(String[] args){
        
        accountManager accountmanager = new accountManager();
        feedbackManager feedback = new feedbackManager();
        medicineManager medicine = new medicineManager();
        patientManager history = new patientManager();
        //create and add new logins
        logins loginOne = new logins("A0001", "bestadminever7");
        accountmanager.addLogin(loginOne);        
        logins loginTwo = new logins("D0001", "bigbeardboi");
        accountmanager.addLogin(loginTwo);        
        logins loginThree = new logins("D0002", "outatime");
        accountmanager.addLogin(loginThree);                       
        logins loginFour = new logins("D0003", "buffalo");  
        accountmanager.addLogin(loginFour);              
        logins loginFive = new logins("S0001", "verysecurepassword");
        accountmanager.addLogin(loginFive);        
        logins loginSix = new logins("P0001", "1234");
        accountmanager.addLogin(loginSix);
        logins loginSeven = new logins("P0002", "mynameisdug");
        accountmanager.addLogin(loginSeven);
        logins loginEight = new logins("P0003", "idunno");
        accountmanager.addLogin(loginEight);      
        //create and add new admins       
        admins adminOne = new admins("Hal", "Jordan", "25 Emerso Road, Kippford, DG5 4SY", "A0001");
        accountmanager.addAdmin(adminOne);       
        //create and add new doctors        
        doctors doctorOne = new doctors("Hugo", "Strange", "Arkham Asylum, Gotham, New Jersey", "D0001");
        accountmanager.addDoctor(doctorOne);         
        doctors doctorTwo = new doctors("Emmett", "Brown", "1640 Riverside Drive, Hill Valley, California",  "D0002");
        accountmanager.addDoctor (doctorTwo);        
        doctors doctorThree = new doctors("The", "Doctor", "Blue Box, Whenever Whereever Street", "D0003");
        accountmanager.addDoctor (doctorThree);        
        //create and add new secretary        
        secretary secretaryOne = new secretary("Pam", "Beesly", "Dunder Mifflin, Scranton, Pennsylvania", "S0001");
        accountmanager.addSecretary (secretaryOne);               
        //create and add new patients
        patients patientOne = new patients("Michael", "Scott", "42 Kellum Court, Scranton, Pennslyvania", "Male", "45", "P0001");
        accountmanager.addPatient(patientOne);        
        patients patientTwo = new patients("Dug", "Person", "I live here, street street", "Male",  "32", "P0002");
        accountmanager.addPatient(patientTwo);        
        patients patientThree = new patients("Jane", "Doe", "Bleeker Street", "Female", "22", "P0003");
        accountmanager.addPatient(patientThree);
           
        doctorFeedback feedbackOne = new doctorFeedback("D0001", "Strange", 4, "His hands are too cold");
        feedback.addFeedback(feedbackOne);
        doctorFeedback feedbackTwo = new doctorFeedback("D0002", "Brown", 3, "Said my arms were an appealing length, made me feel uncomfortable");
        feedback.addFeedback(feedbackTwo);
        
        medicines medicineOne = new medicines("Amoxicillin", 300, "4 per Day - at least 6 hours between each dose");
        medicine.addMedicine(medicineOne);
        medicines medicineTwo = new medicines("Health Potion", 100, "Take 1, will restore to full health");
        medicine.addMedicine(medicineTwo);
        medicines medicineThree = new medicines("Paracetamol", 150, "3 a day, 5 hours between each");
        medicine.addMedicine(medicineThree);
        
        patientDetails historyOne = new patientDetails("P0001", "Patient burned their foot on a George Foreman Grill.", "Paracetamol");
        history.addDetails(historyOne);
        
        //run the login page
        loginPage mr=new loginPage();
        mr.setVisible(true);
        
        
        
       
       
 
       
    }
    
    
}
