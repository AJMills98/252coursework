/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author Aidan
 */
public class patientDetails {
    private String patientID;
    private String patientNotes;
    private String patientPrescription;

    public patientDetails(String patientID, String patientNotes, String patientPrescription) {
        this.patientID = patientID;
        this.patientNotes = patientNotes;
        this.patientPrescription = patientPrescription;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getPatientNotes() {
        return patientNotes;
    }

    public void setPatientNotes(String patientNotes) {
        this.patientNotes = patientNotes;
    }

    public String getPatientPrescription() {
        return patientPrescription;
    }

    public void setPatientPrescription(String patientPrescription) {
        this.patientPrescription = patientPrescription;
    }
    
}
