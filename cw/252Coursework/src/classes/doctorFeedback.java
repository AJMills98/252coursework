/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author Aidan
 */
public class doctorFeedback {
    private String doctorID;
    private String surname;
    private int doctorRating;
    private String doctorFeedback;
    

    public doctorFeedback(String doctorID, String surname, int doctorRating, String doctorFeedback) {
        this.doctorID = doctorID;
        this.surname = surname;
        this.doctorRating = doctorRating;
        this.doctorFeedback = doctorFeedback;
        
    }

    public String getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(String doctorID) {
        this.doctorID = doctorID;
    }

    public int getDoctorRating() {
        return doctorRating;
    }

    public void setDoctorRating(int doctorRating) {
        this.doctorRating = doctorRating;
    }

    public String getDoctorFeedback() {
        return doctorFeedback;
    }

    public void setDoctorFeedback(String doctorFeedback) {
        this.doctorFeedback = doctorFeedback;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

   
    
    
    
}
